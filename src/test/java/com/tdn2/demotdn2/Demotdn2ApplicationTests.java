package com.tdn2.demotdn2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Demotdn2ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
