package com.tdn2.demotdn2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demotdn2Application {

	public static void main(String[] args) {
		SpringApplication.run(Demotdn2Application.class, args);
	}

}
